const imgs = {
  'proj-1': {
    img: './img/dumb/julien-lanoy-1095142-unsplash.jpg',
    color: '#8398ab'
  },
  'proj-2': {
    img: './img/dumb/alex-smith-688708-unsplash.jpg',
    color: '#797574'
  },
  'proj-3': {
    img: './img/dumb/ilnur-kalimullin-332587-unsplash.jpg',
    color: '#686368'
  },
  'proj-4': {
    img: './img/dumb/daniel-korpai-1101671-unsplash.jpg',
    color: '#c6c0c5'
  },
  'proj-5': {
    img: './img/dumb/nordwood-themes-180852-unsplash.jpg',
    color: '#baa980'
  },
  'proj-6': {
    img: './img/dumb/scott-webb-378779-unsplash.jpg',
    color: '#ea202c'
  }
};

// MENU
let menuPopoverState = false;
let menuState = true;
let overlay;
const menu   = document.querySelector('.menu');
const menuBg = document.querySelector('.menu-bg');
const closer = document.querySelector('.closer');

const headerOverlay = document.querySelector('.fade-overlay');
const headerHeight = document.querySelector('.header-container').getBoundingClientRect().height;

const templateMenu = `
<div class="menu-overlay">
  <ul class="projects-list">
    <li id="proj-1">Альфа Банк</li>
    <li id="proj-2">PEPSICO</li>
    <li id="proj-3">LEROY MERLIN</li>
    <li id="proj-4">РЖД</li>
    <li id="proj-5">Дочки-Сыночки</li>
    <li id="proj-6">Globus</li>
  </ul>
  <div id="menu-project-preview"></div>
</div>`;

// ховер менюшки
const menuBgShow = e => {
  // если вверху экрана, то клозер белый, при ховере на меню — черный. при маусауте – белый
  // if (window.pageYOffset <= headerHeight / 2) {
    if (e.clientX >= document.documentElement.clientWidth - menuBg.getBoundingClientRect().width) {
      menuBg.style.backgroundColor = 'var(--color-white)';
      closer.style.fill = 'var(--color-black)';
    } else {
      menuBg.style.backgroundColor = '';
      // если менюшка открыта
      if (menuPopoverState) closer.style.fill = 'var(--color-black)';
      else closer.style.fill = '';
    }
  // если ниже первого экрана, то клозер черный, при ховере на меню — черный. при маусауте все еще черный
  // } else {
  //   if (e.clientX >= document.documentElement.clientWidth - menuBg.getBoundingClientRect().width) {
  //     menuBg.style.backgroundColor = 'var(--color-white)';
  //     closer.style.fill = 'var(--color-black)';
  //   } else {
  //     menuBg.style.backgroundColor = '';
  //     closer.style.fill = 'var(--color-black)';
  //   }
  // }
  let myCloser;
  // если попали на свг или путь внутри него и есть класс closer
  if (e.target.tagName == 'svg' && e.target.classList.contains('closer') ||
      e.target.tagName == 'path' && e.target.parentNode.classList.contains('closer')) {
    // если свг
    if (e.target.tagName == 'svg') {
      e.target.style.fill = 'var(--color-gold)';
      myCloser = e.target;
    // если путь
    } else if (e.target.tagName == 'path') {
      e.target.parentNode.style.fill = 'var(--color-gold)';
      myCloser = e.target.parentNode;
    }
  // если вне свг или пути и до этого были над свг
  } else if (myCloser && myCloser.tagName == 'svg') {
    myCloser.style.fill = '';
  // если вне свг или пути и до этого были над путем
  } else if (myCloser && myCloser.tagName == 'path') {
    myCloser.parentNode.style.fill = '';
  }
};

// остановка и запуск прокрутки окна после открытия галереи
let offset = 0;
const __stopScrolling = () => {
  scrollTo(0, offset);
};
// запуск листнера прокрутки
const immobilizeBody = () => {
  offset = window.pageYOffset || document.documentElement.scrollTop;
  document.addEventListener('scroll', __stopScrolling);
};
// остановка листнера прокрутки
const demobilizeBody = () => {
  document.removeEventListener('scroll', __stopScrolling);
};

// колбэк листнера открытия меню
const menuOpen = () => {
  if (!menuPopoverState) {
    // если внизу экрана
    if (!menuState) {
      menu.style.opacity = '';
      menu.style.display = '';
    }
    document.body.insertAdjacentHTML('afterbegin', templateMenu);
    overlay = document.querySelector('.menu-overlay');
    overlay.style.opacity = '0';
    immobilizeBody();
    setTimeout(() => overlay.style.opacity = '', 50);
    closer.style.transform = 'rotate(180deg) translate(1px, -4px)';
    menuPopoverState = !menuPopoverState;
    const projList = document.querySelector('.projects-list');
    projList.addEventListener('mouseover', showMenuPreview);
    projList.addEventListener('mouseout', removeMenuPreview);
    // закрытие меню
  } else {
    const overlay = document.querySelector('.menu-overlay');
    overlay.style.opacity = '0';
    demobilizeBody();
    const projList = document.querySelector('.projects-list');
    projList.removeEventListener('mouseover', showMenuPreview);
    projList.removeEventListener('mouseout', removeMenuPreview);
    setTimeout(() => {
      overlay.remove();
      closer.style.transform = '';
      menuPopoverState = !menuPopoverState;
      // если внизу экрана
      if (!menuState) {
        menu.style.opacity = '0';
        menu.style.display = 'none';
      }
    }, 150);
  }
};
// показ превьюшки проекта
const showMenuPreview = e => {
  const rndm = () => Math.random();
  const container = document.getElementById('menu-project-preview');
  const overlay = document.querySelector('.menu-overlay');

  container.style.backgroundImage = `url(${imgs[e.target.id].img})`;
  overlay.style.backgroundColor = `${imgs[e.target.id].color}`;
  let width = rndm() * 100;
  if (width < 30) width = 30;
  container.style.width = `${width}vw`;
  let height = rndm() * 100;
  if (height < 30) height = 30;
  container.style.height = `${height}vh`;
  const listItem = e.target.getBoundingClientRect();
  container.style.top = `${(listItem.top + listItem.height / 2) * 100 / document.documentElement.clientHeight}%`;
};
const removeMenuPreview = e => {
  const container = document.getElementById('menu-project-preview');
  const overlay = document.querySelector('.menu-overlay');
  container.style.backgroundImage = '';
  container.style.width = '';
  container.style.height = '';
  container.style.top = '';
  overlay.style.backgroundColor = '';
};

document.addEventListener('mousemove', menuBgShow);
closer.addEventListener('click', menuOpen);


// HEADER OVERLAY
const fadeHeaderOverlay = () => {
  let scroll = window.pageYOffset;
  let opacity = Math.round(scroll * 100 / (headerHeight - 100)) / 100;
  if (opacity <= 1) headerOverlay.style.opacity = `${opacity}`;
  // перекраска клозера
  // if (window.pageYOffset > headerHeight / 2) closer.style.fill = 'var(--color-black)';
  // else closer.style.fill = '';
};
window.addEventListener('scroll', fadeHeaderOverlay);



// LAZY CONTENT
const lazy = document.querySelectorAll('.lazy-show');

const lazyContent = () => {
  let bodyHeight = Math.round(document.body.getBoundingClientRect().height);
  let docHeight  = document.documentElement.clientHeight;
  let offset     = window.pageYOffset;
  lazy.forEach(el => {
    // раздупляем позицию эемента во вьюхе
    let top = el.getBoundingClientRect();
    // если элемент выше на Xрх от нижнего края вьюхи
    if (document.documentElement.clientHeight - top.top > 50) {
      el.style.opacity = '1';
      el.style.transform = 'translateY(0)';
    }
  });
  if ((offset + docHeight) >= bodyHeight - 100) {
    menu.style.opacity = '0';
    setTimeout(() => menu.style.display = 'none', 300);
    menuState = !menuState;
  } else {
    menu.style.display = '';
    setTimeout(() => menu.style.opacity = '', 50);
    menuState = !menuState;
  }
};
window.addEventListener('scroll', lazyContent);



// PROJECT NAME
// let mapProj = [];
// let iii = 1;
// while (document.getElementById(`main-proj-${iii}`) !== null) {
//   mapProj.push(document.getElementById(`main-proj-${iii}`));
//   iii++;
// }
// const projectName = e => {
//   const color = {
//     'main-proj-1':'#8398ab',
//     'main-proj-2':'#797574',
//     'main-proj-3':'#686368',
//     'main-proj-4':'#3B2F2F'
//   };
//   let docHeight  = document.documentElement.clientHeight;

//   let rect;
//   mapProj.forEach(item => {
//     rect = item.getBoundingClientRect();
//     if (item.id == 'main-proj-1') console.log(`${rect.top} - ${rect.bottom}`);
//     if (docHeight / 3 >= rect.top && docHeight / 3 * 2 <= rect.bottom) {
//       document.body.style.backgroundColor = `${color[item.id]}`;
//     } else document.body.style.backgroundColor = '';
//   });
// };
// window.addEventListener('scroll', projectName);

/**

// const cnvs = document.getElementById('cnvs');
const ctx = cnvs.getContext('2d');

let cnvsWidth = document.body.getBoundingClientRect().width;
let cnvsHeight = document.body.getBoundingClientRect().width / 4;

// размеры канваса во вьюхе для ретины
let width  = cnvs.width  = cnvsWidth * 2;
let height = cnvs.height = cnvsHeight * 2;
// физические размеры канваса
cnvs.style.width  = `${cnvsWidth}px`;
cnvs.style.height = `${cnvsHeight}px`;

// сетка
const grid = {
  big:   [],
  small: [],
};

// размеры секторов
let sector = {
  big: {
    step: height,
    grid: {
      column: width / height,
      row:    height / height,
    }
  },
  small: {
    step: height / 2,
    grid: {
      column: width / (height / 2),
      row:    height / (height / 2),
    }
  }
};

// данные треугольников для каждого сектора в сетке
const setGrid = type => {
  let x, y;
  for (y = 0; y < sector[type].grid.row; y++) {
    for (x = 0; x < sector[type].grid.column; x++) {
      grid[type].push({
        topLeft: [
          { x: x * sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step,
            y: y * sector[type].step + sector[type].step },
        ],
        topRight: [
          { x: x * sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step + sector[type].step },
        ],
        bottomRight: [
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step + sector[type].step },
          { x: x * sector[type].step,
            y: y * sector[type].step + sector[type].step },
        ],
        bottomLeft: [
          { x: x * sector[type].step,
            y: y * sector[type].step },
          { x: x * sector[type].step + sector[type].step,
            y: y * sector[type].step + sector[type].step },
          { x: x * sector[type].step,
            y: y * sector[type].step + sector[type].step },
        ]
      });
    }
  }
};

// отрисовка сетки. size — размер сектора, type — поворот треугольника
const drawGrid = (size, type) => {
  grid[size].forEach(pixel => {
    ctx.beginPath();
    pixel[type].forEach(line => {
      ctx.lineTo(line.x, line.y);
    });
    ctx.closePath();
    ctx.fillStyle = 'black';
    ctx.fill();
  });
};

const reDraw = () => {
  cnvsWidth = document.body.getBoundingClientRect().width;
  cnvsHeight = document.body.getBoundingClientRect().width / 4;
  // размеры канваса во вьюхе для ретины
  width  = cnvs.width  = cnvsWidth * 2;
  height = cnvs.height = cnvsHeight * 2;
  // физические размеры канваса
  cnvs.style.width  = `${cnvsWidth}px`;
  cnvs.style.height = `${cnvsHeight}px`;
  // размеры секторов
  sector = {
    big: {
      step: height,
      grid: {
        column: width / height,
        row:    height / height,
      }
    },
    small: {
      step: height / 2,
      grid: {
        column: width / (height / 2),
        row:    height / (height / 2),
      }
    }
  };
  // заполнение сетки данными секторов
  Object.keys(grid).forEach(type => setGrid(type));
  // отрисовка
  // ctx.clearRect(0,0,cnvs.width,cnvs.height);
  drawGrid('small', 'topLeft');
};


// заполнение сетки данными секторов
Object.keys(grid).forEach(type => setGrid(type));
// отрисовка
drawGrid('small', 'topLeft');

window.addEventListener('resize', reDraw);

 */